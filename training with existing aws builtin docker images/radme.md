data hierarchies for built aws model classification
s3://your-bucket-name/
    |--- train/
    |     |--- fight/
    |     |    |--- fight_image1.jpg
    |     |    |--- fight_image2.jpg
    |     |    |--- ...
    |     |
    |     |--- no_fight/
    |          |--- no_fight_image1.jpg
    |          |--- no_fight_image2.jpg
    |          |--- ...
    |
    |--- val/
    |     |--- fight/
    |     |    |--- fight_image101.jpg
    |     |    |--- fight_image102.jpg
    |     |    |--- ...
    |     |
    |     |--- no_fight/
    |          |--- no_fight_image101.jpg
    |          |--- no_fight_image102.jpg
    |          |--- ...
    |
    |--- train.lst
    |--- val.lst
